<h1 align = "center">IJERT Journal Template</h1>

<b>Connect with me at:</b>
<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>

**International Journal of Engineering Research and Technology (IJERT)** is a *peer-reviewed*, *open-access* and *multi-disciplinary* Journal. Here, in this Repository, I present a simple-minimal <img src="https://latex.codecogs.com/gif.latex?\text { \LaTeX } " /> template for this Journal.

*You are free to do anything with this template, and change the same according to your need!*

## Project Release(s) & Details
Currently, this <img src="https://latex.codecogs.com/gif.latex?\text { \LaTeX } " /> Document has two Formatting Styles, as follows:
### Release v1.x
- Supports Single Author Name,
- Author Details (and Affiliations) are in the Second Line.

### Release v2.x (Upcoming)
- Supports Multiple Authors,
- Each Author, with their TAGs are in the following Line (as Required in the Journal).